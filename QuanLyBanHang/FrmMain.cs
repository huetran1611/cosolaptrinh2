﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void dmHang_DoubleClick(object sender, EventArgs e)
        {
            FrmDMHangHoa fHang = new FrmDMHangHoa();
            fHang.Show();
        }
    }
}
