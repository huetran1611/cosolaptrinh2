﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class FrmDMHangHoa : Form
    {
        SqlConnection con = new SqlConnection();
        public FrmDMHangHoa()
        {
            InitializeComponent();
        }

        private void FrmDMHangHoa_Load(object sender, EventArgs e)
        {
            string connectionString = "Data Source=localhost\\SQLEXPRESS;Initial Catalog=QuanLyBanHang;Integrated Security=True";
            con.ConnectionString = connectionString;
            con.Open();

            loadDataToGridview();

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            //Delete From DMHang Where MaHang = '001'

            // Tao cau lenh xoa:
            string sql = "Delete From DMHang Where MaHang = '" + txtMaHang.Text + "'";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            loadDataToGridview();

        }

        private void loadDataToGridview()
        {
            string sql = "Select * From DMHang";
            SqlDataAdapter adp = new SqlDataAdapter(sql, con);

            DataTable tableDMHang = new DataTable();
            adp.Fill(tableDMHang);

            DataGridView_DMHang.DataSource = tableDMHang;
        }

        private void DataGridView_DMHang_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtGiaBan.Text = DataGridView_DMHang.CurrentRow.Cells["GiaBan"].Value.ToString();
            txtGiaNhap.Text = DataGridView_DMHang.CurrentRow.Cells["GiaNhap"].Value.ToString();
            txtMaHang.Text = DataGridView_DMHang.CurrentRow.Cells["MaHang"].Value.ToString();
            txtSoLuong.Text = DataGridView_DMHang.CurrentRow.Cells["SoLuong"].Value.ToString();
            txtTenHang.Text = DataGridView_DMHang.CurrentRow.Cells["TenHang"].Value.ToString();
            txtMaHang.Enabled = false;


        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            txtGiaBan.Text = "";
            txtGiaNhap.Text = "";
            txtMaHang.Text = "";
            txtSoLuong.Text = "";
            txtTenHang.Text = "";
            txtMaHang.Enabled = true;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if(txtMaHang.Text == "")
            {
                MessageBox.Show("bạn cần nhập mã hàng");
                txtMaHang.Focus();
                return;
            }
            if(txtTenHang.Text == "")
            {
                MessageBox.Show("Bạn cần nhập tên hàng");
                txtTenHang.Focus();
            }
            else
            {
                // insert into DMHang values ()
                string sql = "insert into DMHang values ('" + txtMaHang.Text + "', '" + txtTenHang.Text + "'";
                if (txtGiaNhap.Text != "")
                    sql = sql + ", " + txtGiaNhap.Text.Trim();
                if (txtGiaBan.Text != "")
                    sql = sql + ", " + txtGiaBan.Text.Trim();
                if (txtSoLuong.Text != "")
                    sql = sql + "," + txtSoLuong.Text.Trim();
                sql = sql + ")";
                ///MessageBox.Show(sql);
                try
                {
                    SqlCommand cmd = new SqlCommand(sql, con);
                    cmd.ExecuteNonQuery();

                    loadDataToGridview();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                                            
                return;
                
                
            }

        }

        private void txtGiaNhap_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (((e.KeyChar >= '0') && (e.KeyChar <= '9')) ||
              (Convert.ToInt32(e.KeyChar) == 8) || (Convert.ToInt32(e.KeyChar) == 13))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void btnĐóng_Click(object sender, EventArgs e)
        {
            con.Close();
            this.Close();
        }
    }
}
