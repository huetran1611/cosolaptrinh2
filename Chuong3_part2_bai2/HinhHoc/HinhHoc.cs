﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chuong3_part2_bai2.HinhHoc
{
    abstract public class HinhHoc
    {
        abstract public void nhap();
        abstract public double chuVi();
        abstract public double dienTich();
    }
}
