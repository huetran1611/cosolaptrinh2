﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chuong3_part3_bt1
{
    class mangSV
    {
        private int SoLuong;
        private SinhVien[] Danhsach;

        public void nhap()
        {
            Console.WriteLine("hay nhap so luong sinh vien: ");
            SoLuong = Convert.ToInt32(Console.ReadLine());
            Danhsach = new SinhVien[SoLuong];

            for (int i=0; i<SoLuong; i++)
            {
                Danhsach[i] = new SinhVien();
                Danhsach[i].nhap();
            }
        }

        public int SoluongSinhVienChuyenDe()
        {
            int dem = 0;

            for(int i =0; i<SoLuong; i++)
            {
                if (Danhsach[i].KieuTotNghiep() == 2)
                    dem = dem + 1;
            }
            return dem;

        }
        public int SoLuongSinhVienKhoaLuan()
        {
            int dem = 0;
            for (int i=0; i<SoLuong; i++)
            {
                if (Danhsach[i].KieuTotNghiep() == 1)
                    dem = dem + 1;
            }
            return dem;
        }
    }
}
